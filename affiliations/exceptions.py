"""Custom exceptions for the affiliations application."""


class InvalidCallBackURL(Exception):
    """Exception for invalid callback for Subscription."""

    pass
