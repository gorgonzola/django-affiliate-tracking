"""Tests for the affiliations application."""
from .test_models import *
from .test_apps import *
from .test_affiliations_settings import *
from .test_middleware import *
from .test_utils import *
from .test_triggers import *
