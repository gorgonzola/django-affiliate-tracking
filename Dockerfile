FROM ubuntu:xenial
MAINTAINER Mikkel Munch Mortensen <mim@saxo.com>
RUN apt-get update && apt-get install -y python-virtualenv python3-dev gcc git git-core pkg-config
