#!/bin/bash

CODE_TO_LINT='manage.py dummy_project affiliations'
CODE_TO_IGNORE_LINT='*/migrations/*'

INITIAL_DIR=$PWD
cd `git rev-parse --show-toplevel`

if [ "$1" == "create-env" ]; then
    virtualenv env
    source env/bin/activate
    pip install -r requirements/ci.txt
fi

pycodestyle $CODE_TO_LINT --exclude=$CODE_TO_IGNORE_LINT
CODE_STATUS=$?
pydocstyle $CODE_TO_LINT
DOC_STATUS=$?

if [ "$1" == "create-env" ]; then
    rm -rf env
fi

cd $INITIAL_DIR

if [ "$CODE_STATUS" == 1 ] || [ "$DOC_STATUS" == 1 ] || [ "$CODE_STATUS" == 127 ] || [ "$DOC_STATUS" == 127 ]; then
    exit 1
else
    echo "No linting errors \\o/"
    exit 0
fi
