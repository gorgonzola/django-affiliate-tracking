#!/bin/bash

# Define which directories and files to look for tests in.
CODE_TO_TEST='affiliations'

# Remember what directory we are in.
INITIAL_DIR=$PWD
cd `git rev-parse --show-toplevel`

# Define some defaults.
CREATE_ENV=0
PYTHON=""
DJANGO_VERSION_BELOW=""

# Parse options.
while getopts ":ep:d:" OPT; do
    case $OPT in
        # Whether to create an virtualenv.
        e)
            CREATE_ENV=1
            ;;
        # Use custom Python executable.
        p)
            PYTHON=$OPTARG
            ;;
        # Use custom Django version. The version just above the desired
        # version should be assigned, to make sure we use most recent minor
        # version of desired version (e.g. specify `1.11` to use most recent
        # `1.10.x`).
        d)
            DJANGO_VERSION_BELOW=$OPTARG
            ;;
    esac
done

# Decide whether to create a virtualenv.
if [ "$CREATE_ENV" == "1" ]; then
    # Decide which Python executable to use.
    if [ "$PYTHON" != "" ]; then
        virtualenv testenv --python=`which $PYTHON`
    else
        virtualenv testenv --python=`which python2`
    fi
    # Activate and install basic dependencies.
    source testenv/bin/activate
    pip install -r requirements/ci.txt
    # Install custom Django version, if desired.
    if [ "$DJANGO_VERSION_BELOW" != "" ]; then
        pip install --upgrade django\<$DJANGO_VERSION_BELOW
    fi
fi

# Run tests.
coverage run --append ./manage.py test $CODE_TO_TEST
TEST_STATUS=$?

# Generate coverage report if no custom Python or Django version is specified.
if [ "$PYTHON" == "" ] && [ "$DJANGO_VERSION_BELOW" == "" ]; then
    REPORT=`coverage report | grep -P "\d+\.\d+\%$"`
    COVERAGE_STATUS=${REPORT##* }
fi

# Remove virtualenv, if created.
if [ "$CREATE_ENV" == "1" ]; then
    rm -rf testenv
fi

# Return to initial directory.
cd $INITIAL_DIR

# Return if tests failed.
if [ "$TEST_STATUS" != 0 ]; then
    exit $TEST_STATUS
fi

# Output coverage total if no custom Python or Django version is specified.
if [ "$PYTHON" == "" ] && [ "$DJANGO_VERSION_BELOW" == "" ]; then
    if [ "$COVERAGE_STATUS" != "100.00%" ]; then
        echo "Test coverage is less than 100%."
        echo "Coverage total: $COVERAGE_STATUS"
        exit 1
    fi

    echo "Coverage total: $COVERAGE_STATUS"
# Give an indication of why the coverage status is not being output.
else
    echo "Custom Python and/or Django version. Coverage not run."
fi
